/*
libfsm

Copyright (c) 2020 Michael Ambrus

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef fsm_h
#define fsm_h

#include <stdint.h>

/* fsm_action:

    Main struct for everything that executes in a FSM. I.e

    - State entry action
    - State exit action
    - Trigger detection
    - FSM run-time internals

    One function and it's arguments. Function takes arguments as void* and
    cast them internally. Function returns true/false based on success. If
    trigger, success means trigger is detected.

    NOTE: No action must EVER block. The whole concept is to DO or to
    DETERMINE.

 */
struct fsm_action {
    int (*func) (void *args);
    void *arg;
};

/* fsm_trigger:

    A trigger is an action with the role to detect some form of condition
    (formally called "event) and associate that condition with a state.

    functions instead of direct expressions:

    - Relieve need of global variables
    - Vastly improve readability as one function-name represent the whole
      condition
    - As conditions are actions, condition readability is encouraged by
      nature which decreases complexity. I.e. increase quality, ease
      maintainability.
 */
struct fsm_state;               /* forward declaration */
struct fsm_trigger {
    int next_state;             /* Next state-ID if condition true */
    struct fsm_action *detect;  /* Detect trigger-condition */
};

/* fsm_state:

   Formal Finite State Machine (FSM) state (finite automata). This is the
   UML extended representation with entry/exit functions. Hatley & Pirbhai
   representation is achieved by keeping only the trigger-list (i.e. set
   entry/exit to NULL)
 */
struct fsm_state {
    int state_id;               /* Assign from local enum type,
                                   cast if symbolic names needed (debugger) */
    struct fsm_action *entry;   /* States entry-action */
    struct fsm_action *exit;
    struct fsm_trigger **triggers;  /* Trigger-vector. Tested in order. First one
                                       that returns true holds the next_state */
};

/* Name-space obfuscater: FSM_NAME

   The main purpose of the FSM_NAME is to avoid name-collation with
   application name-space.

   Defining FSM_NAME prior including this header however file permits
   re-use of the same symbolic state-names in more than one state-machine
   for the same program. Re-using the same symbolic names will otherwise
   lead to link-errors enforcing different names. Preferably state-machines
   are simple, one for each thread managing a system-isolated domain, like
   "battery", or device driver for either specific device-drivers
   (integrated circuit) or some higher level abstraction. The "battery"
   domain mentioned for example may use a high-abstraction "GPIO" driver,
   which in turn uses a abstract "i2c" driver, which in turn uses a specific
   circuit driver. Each of these would have state-machines, but in good
   design these are separated and independent of each other, which in tun
   makes each FSM very simple, i.e. usually consisting of only less than a
   hand-full states.  multi-threaded programs with parallel high-level FSM:s
   the likely-hood is high (consider high-level state-names {on,off} or
   {running, error}
*/
#ifndef FSM_NAME
#define FSM_NAME(N) N ## __DEFLTMACHINE
#endif

/* Declare action-function. Assures correct return type and argument variable
   name. */
#define FSM_FUN( ACTION )                                       \
   int FSM_NAME(_fsm_action_ ## ACTION)(void *__varg)

/* Declare action. I.e function and named action argument-struct */
#define FSM_ACTION( ACTION, ARG )                               \
    FSM_FUN( ACTION );                                          \
    struct FSM_NAME(_fsm_action_arg_ ## ACTION) ARG ;

/* Expand to action argument type */
#define FSM_ARG_T( ACTION ) struct FSM_NAME(_fsm_action_arg_ ## ACTION)

/* Refer to a action argument */
#define FSM_ARG( ACTION, MEMBER ) ((FSM_ARG_T( ACTION )*)__varg)->MEMBER

/* Define a local variable NAME for action ACTION */
#define FSM_ARG_DEF( ACTION, NAME )                             \
    FSM_ARG_T( ACTION ) *NAME = __varg;

/* Expands to a action declaration */
#define FSM_STATE_ACT( ACTION, ... ) \
    &(struct fsm_action){ FSM_NAME(_fsm_action_ ## ACTION) ,  &(FSM_ARG_T(ACTION)){ __VA_ARGS__ }  }

/* "Trigger", i.e. active event-detector */
#define FSM_TRIG( NEXT_STATE, ACTION, ... ) \
    &(struct fsm_trigger){ NEXT_STATE, FSM_STATE_ACT( ACTION,  ## __VA_ARGS__)}

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof (a) / sizeof ((a)[0]))
#endif

int fsm_action(struct fsm_action *action);
struct fsm_state *fsm_nextstate(struct fsm_state *state, int entry_enable);
int fsm_dispatcher(struct fsm_state *start_state,
                   struct fsm_action *kill_trigger);
void __attribute__ ((weak)) fsm_sync(void);
uint8_t __attribute__ ((weak)) fsm_getport(int port);

struct fsm_state *end_state; /* Pseudo state that terminates dispatcher */
#define ENDSTATE ((signed)0xC0DEDEAD)

#endif                          //fsm_h
