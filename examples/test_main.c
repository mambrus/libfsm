#include <stdio.h>
#include <stdlib.h>
#define FSM_NAME(N) N ## __ANOTHER_MACHINE
#include <fsm/fsm.h>
#include "examples_config.h"
#include "local.h"

FSM_FUN(key_value)
{
    return (FSM_ARG(key_value, key) == fsm_getport(FSM_ARG(key_value, port)));
}

#define TEST_KEY_ACTION( KEY ) \
    if (fsm_action(FSM_STATE_ACT(key_value, KEY))) \
            printf("Key-trigger detect '%c'\n", KEY);

int main(int argc, char **argv)
{
    printf("Welcome to libFSM demo.\n"
           "  Press 1-9 <ENTER>:  change between states where\n"
           "                      FSM rules permit\n"
           "  Press 's' <ENTER>:  Re-enter same state where\n"
           "                      FSM rules permit\n"
           "  Press 'x' <ENTER>:  Exit demo FSM demo\n");
    fsm_dispatcher(state_one_p, FSM_STATE_ACT(key_value, 'x'));

    printf("FSM ACTION \"key_value\" test.\n"
           "  Press any key. Valid keys will print:"
           "  Key-trigger detect\" \n"
           "\n" "  Press <CTRL>-'c' to terminate program\n");

    while (1) {
        fsm_sync();
        TEST_KEY_ACTION('M');
        TEST_KEY_ACTION('i');
        TEST_KEY_ACTION('c');
        TEST_KEY_ACTION('h');
        TEST_KEY_ACTION('a');
        TEST_KEY_ACTION('e');
        TEST_KEY_ACTION('l');
    }

    return 0;
}
