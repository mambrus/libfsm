#ifndef fsm_local_h
#define fsm_local_h

#include <fsm/fsm.h>

/* Shared event detectors (ACTION) */
FSM_ACTION(key_value, {
           int key;
           int port;});

/* Shared entry/exit (ACTION) */
FSM_ACTION(generic_entry, {
           char name[20];
           int roll;
           float marks;
           });

FSM_ACTION(generic_exit, {
           char name[20];
           int born;
           });

/* Forward declare state-pointers. Only states needed start dispatching are
 * needed to be public. I.e. typically start-state */
extern struct fsm_state *state_one_p;

#endif                          //fsm_local_h
