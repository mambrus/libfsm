#include <unistd.h>
#define FSM_NAME(N) N ## __TEST_MACHINE
#include <fsm/fsm.h>
#include "examples_config.h"
#include "local.h"

/* --------------------- State machine logic --------------------- */

enum STATES {
    STATE1 = 0,
    STATE2,
    STATE3
};

static const struct fsm_state mashine[] = {
    {
     STATE1,
     FSM_STATE_ACT(generic_entry, "Enter 1", 67, 78.3),
     FSM_STATE_ACT(generic_exit, "Exit 1", 65),
     (struct fsm_trigger *[]){
                              FSM_TRIG(STATE1, key_value, 's', 0),
                              FSM_TRIG(STATE2, key_value, '2', 0),
                              FSM_TRIG(STATE3, key_value, '3', 0),
                              NULL}
     },
    {
     STATE2,
     FSM_STATE_ACT(generic_entry, "Enter 2", 67, 78.3),
     FSM_STATE_ACT(generic_exit, "Exit 2", 65),
     (struct fsm_trigger *[]){
                              FSM_TRIG(STATE2, key_value, 's', 1),
                              FSM_TRIG(STATE1, key_value, '1', 1),
                              NULL}
     },
    {
     STATE3,
     FSM_STATE_ACT(generic_entry, "Enter 3", 67, 78.3),
     FSM_STATE_ACT(generic_exit, "Exit 3", 65),
     (struct fsm_trigger *[]){
                              FSM_TRIG(STATE3, key_value, 's', 0),
                              FSM_TRIG(STATE2, key_value, '2', 0),
                              NULL}
     }
};

/* -- states end -- */

/* Starter state-pointers */
struct fsm_state *state_one_p = (struct fsm_state *)&(mashine[STATE1]);
