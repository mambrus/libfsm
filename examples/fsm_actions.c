#define FSM_NAME(N) N ## __TEST_MACHINE
#include <fsm/fsm.h>
#include <stdio.h>
#include "examples_config.h"
#include "local.h"

/* Executing code (ACTIONS) are separated from the logic */

FSM_FUN(key_value)
{
    return (FSM_ARG(key_value, key) == fsm_getport(FSM_ARG(key_value, port)));
}

FSM_FUN(generic_entry)
{
    FSM_ARG_DEF(generic_entry, arg);

    if (arg->roll == 67) {
        printf("%s\n", arg->name);
    }
    return 1;
}

FSM_FUN(generic_exit)
{
    if (FSM_ARG(generic_exit, born) == 65) {
        printf("%s\n", FSM_ARG(generic_exit, name));
    }
    return 1;
}
