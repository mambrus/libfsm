/*
libfsm

Copyright (c) 2020 Michael Ambrus

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include "local.h"
#define __init __attribute__((constructor))
#define __fini __attribute__((destructor))

/*
 * If libfsm is built and used as dynamic library, this is undefined,
 * therefore we set it to something recognizable */
#ifndef FSM_AS_PROCESS_NAME_DFLT
#define FSM_AS_PROCESS_NAME_DFLT "FSM-DYNAMIC-LIBRARY"
#endif

/* Module initializers/de-initializers. When used as library (who don't have
 * a natural entry/exit function) these are used to initialize
 * de-initialize. Use to set predefined/default states and cleanup.
 *
 * This will work with shared libraries as well as with static as they get
 * invoked by RTL load/unload, with or without C++ code (i.e. functions will
 * play nice with C++ normal ctors/dtors).
 *
 * Keep fsm in to at least once per new build-/run-environment assert that
 * the mechanism works.
 */

void __init __libfsm_init(void)
{
#ifdef ENABLE_INITFINI_SHOWEXEC
    fprintf(stderr, "%s [%s]: initializing for: %s\n", PROJ_NAME, VERSION,
            FSM_AS_PROCESS_NAME_DFLT);
    fflush(NULL);
#endif
}

void __fini __libfsm_fini(void)
{
#ifdef ENABLE_INITFINI_SHOWEXEC
    fprintf(stderr, "%s [%s]: de-initializing for: %s\n", PROJ_NAME, VERSION,
            FSM_AS_PROCESS_NAME_DFLT);
    fflush(NULL);
#endif
}
