/*
libfsm

Copyright (c) 2020 Michael Ambrus

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <strings.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include <fsm/fsm.h>

/* Execute an action with arguments and return it's result */
int fsm_action(struct fsm_action *action)
{
    return action != NULL ? action->func(action->arg) : -1;
}

/* Find out which is the next state, optionally running entry/exit actions

    - Running entry function is conditional from outside and iif not NULL
    - Running exit function is done if event is detected by executing
      states list of event.-detectors (triggers).

    If condition for new state is detected, this state is returned, otherwise
    NULL to let caller know.
*/
struct fsm_state *fsm_nextstate(struct fsm_state *state, int entry_enable)
{
    struct fsm_state *next_state = NULL;
    struct fsm_trigger *curr_trigger =
        state->triggers ? state->triggers[0] : NULL;
    int trigged = 0;
    int idx = 0;
    struct fsm_state *machine = state - (state->state_id);

    if (entry_enable) {
        state->entry ? fsm_action(state->entry) : -1;
    }

    while (curr_trigger && !trigged) {
        trigged = fsm_action(curr_trigger->detect);
        if (trigged) {
            next_state =
                curr_trigger->next_state != ENDSTATE ?
                &(machine[(curr_trigger->next_state)]) : end_state;
        }
        idx++;
        curr_trigger = state->triggers[idx];
    }
    if (next_state) {
        state->entry ? fsm_action(state->exit) : -1;
    }
    return next_state;
}

/* Dispatch a state-machine, starting from "start-state"

    - start-state can be any from a inter-connected rule-set
    - No action must block
    - fsm_sync is weakly linked and intended to be replaced
      by applications, function-body is not required.
 */
int fsm_dispatcher(struct fsm_state *start_state,
                   struct fsm_action *kill_trigger)
{
    struct fsm_state *curr_state = start_state;
    struct fsm_state *next_state = NULL;
    int state_changed = 1;      /* fsm_nextstate can run /entry */

    while (!(kill_trigger ? fsm_action(kill_trigger) : 0)
           && (curr_state != end_state)) {
        fsm_sync();
        next_state = fsm_nextstate(curr_state, state_changed);
        if (next_state != NULL) {
            state_changed = 1;
            curr_state = next_state;
        } else {
            state_changed = 0;
        }
    }
    return 1;
}

/*
 Unique pseudo state that will never run. We need a value guaranteed to be
 unique, therefore an address to a complete state-variable is used. Could in
 principle be initialized with anything but only state_id will ever be
 considered.
 */
struct fsm_state *end_state = &(struct fsm_state) {
    .state_id = ENDSTATE,
    .entry = NULL,
    .exit = NULL,
    .triggers = (struct fsm_trigger *[]) {NULL}
};

#include <fsm/fsm.h>
