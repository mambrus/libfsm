# libfsm

Finite State Machines - in `C`

* `libfsm` is a rigid framework for implementing formal state-machines
  from either `UML2.5`, or as `UML` state-machines derives from much older
  semantics, like Mealy & Moore machines etc.
* `libfsm` is the bottom half of `fsm-cc` which is a `FSM` compiler. I.e. it
  has no fluff and is very "machinable".
* Manually implementing a `FSM` under `libfsm` is supported as great
  emphasis on readability is one of the design criteria (see `/example/`
  directory). I.e. compiling a `FSM` is not needed unless for practical
  reasons - like actual complexity (many states, parallel `FSM`'s etc).
* Mentioning of `UML` does not mean that `libfsm` requires C++ or
  object orientation. Whether or not usage (i.e. application) is based on
  `C++` is agnostic to `libfsm` and will remain so. I.e. you can use
  either, `libfsm` enforces neither.
* Design primarily addresses deeply embedded resource and fast-execution
  usages. Embedded SW can potentially scale from embedded to more complete
  computers. The opposite is practically never true. Even if it can be done
  there's always a penalty. `libfsm` runs on from bare-silicon up. The
  example `/example/` directory is for desktop computers to prove the point.
* Integration
  * Source-integration in embedded projects are easy. Just include libfsm.c
    and add a build include-path to `include/`. I.e. adapting to `libfsm`'s
    build-system is not needed.
  * For real computers (like Linux), integration is done with libraries as
    normal. Installing the package and adding `-llibfsm` to the final
    link-stage is enough.
* All structures are resolved in build time. This means truth-table etc.
  can favourably be placed in `RO` memory, making the final result *as rigid
  as a any manually implemented state-machine*.
* Name-spacing allows implementing several `FSM's` using the same
 `libfsm`-library in the same application.

Official repository is here: [gitlab libfsm](https://gitlab.com/mambrus/libfsm)

`libfsm` is FREE SOFTWARE released under the [MIT License](https://gitlab.com/mambrus/libfsm/-/blob/master/LICENSE)



