/*
libfsm

Copyright (c) 2020 Michael Ambrus

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <sys/select.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/* Simulate HW.
 *
 * global variable below an exception to the rule of non-globals
 * ONLY for the sake of simulation, it is meant to represent a HW state
 * without any buffering */

static int g_current_key = -1;

void fsm_sync()
{
    int retval;
    fd_set rfds;
    g_current_key = -1;

    /* Watch stdin (fd 0) to see when it has input. */
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);

    retval = select(1, &rfds, NULL, NULL, &(struct timeval) {
                    .tv_sec = 0,.tv_usec = 1000});
    if (retval == -1) {
        perror("select()");
        exit(1);
    } else if (retval && FD_ISSET(0, &rfds)) {
        g_current_key = getchar();
    }
}

uint8_t fsm_getport(int port)
{
    /* Simulation so we disregard the port in-arg */
    return g_current_key;
}
